<%@ page import="com.hp.itsm.ssp.beans.SdClientBean" %>

<%@ page import="com.hp.itsm.api.interfaces.IServicecallHome" %>
<%@ page import="com.hp.itsm.api.interfaces.IServicecall" %>
<%@ page import="com.hp.itsm.api.interfaces.*" %>
<%@ page import="com.hp.itsm.api.interfaces.IAccount" %>
<%@ page import="com.hp.ifc.attachments.ftp.UUIDUtil" %>

<%@ page import="org.apache.commons.fileupload.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="ru.iteco.sd.ftpdownload.FTPDownloader" %>

<%@ page contentType="text/html; charset=utf-8" %>
<%!
String sdPath(long activityoid) {
        String uuid = UUIDUtil.UUIDtoString(UUIDUtil.makeUUID(activityoid));
        uuid = uuid.replaceAll("-", "");
        char uuidArray[] = uuid.toCharArray();
        char pathArray[] = new char[uuidArray.length + uuidArray.length / 3];
        int i = 0;
        int j = 0;
        for(; i < uuidArray.length; i++) {
            pathArray[j++] = uuidArray[i];
            if((i + 1) % 3 == 0)
                pathArray[j++] = '/';
        }
        return new String(pathArray);
}
String simpleEncoding(String todos){
			Integer[] transposition = {4,7,3,0,6,9,2,15,22,10,1,13,8,14,23,28,16,27,17,25,18,19,5,20,21,24,26,11,29,12 };
			char[] substitution = "PsR5Zi2LNg0Hl(p&S7oUI$YWv#C-3MmOz%^Jkb1,we*VFEd+@TA)t9r4GnQ.qxh6=8ujcXyDKfBa".toCharArray();
			String sym = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM0123456789,.-@#$%^&*()+=";
			char[] inp = todos.toCharArray();			
			int len = (transposition.length > inp.length)?transposition.length: inp.length;
			char[] outp = new char[len];
			Arrays.fill(outp, ' ');
			for (int i = 0; i < inp.length; i++) {			
				int sidx = sym.indexOf(String.valueOf( inp[i]));
				int tidx = (i<transposition.length)?transposition[i]:i;
				outp[tidx] = (sidx == -1)? inp[i] : substitution[sidx];
//				System.out.println("tidx="+tidx+" i="+i+" inp[i]="+String.valueOf( inp[i])+" sidx="+sidx+" out="+(new String(outp)));
			}
			return new String(outp);
}
%>

<HTML>
    <HEAD>
        <LINK rel="stylesheet" type="text/css" href="sd-sp.css">
		<style type="text/css">
		</style>
		<script>
			function toggleSignatures(elem){
				var fid = elem.getAttribute("data-fid");
				var tr = elem.parentElement.parentElement.nextSibling;
				if(tr.style.display == "none"){
						elem.value="Скрыть ЭЦП";						
						tr.style.display ="block";						
						refreshSignatures(fid);
				}else{
						elem.value="Просмотреть ЭЦП";
						tr.style.display ="none";
				}
			}
			function refreshSignatures(el){
				var fid = (el.tagName)? el.getAttribute("data-fid"): el;
				document.getElementById(fid).innerHTML="<br><br><center><img src='images/waiting-icon.gif' /><br><span id='"+fid+"status'></span></center>"; 
				setTimeout( function(){signer.getSignInfo(""+fid);}, 100);
			}
			function sign(el){
				var fid = (el.tagName)? el.getAttribute("data-fid"): el;
				document.getElementById(fid).innerHTML="<br><br><center><img src='images/waiting-icon.gif' /><br><span id='"+fid+"status'></span></center>"; 
				signer.sign(""+fid);
			}
		</script>
    </HEAD>
    <BODY>
	  
      <% 
      final String appserver = getServletContext().getInitParameter("sd_application_server");	
    
      SdClientBean bean = (SdClientBean) session.getAttribute("sd-client-bean");
      response.setHeader("Cache-Control", "no-cache");
      if (bean == null) 
      {
        %><jsp:forward page="NoSessionError.jsp" /><%            
      }
	  
      String _sc = request.getParameter("vSc");
      String _todo = request.getParameter("ToDo");
      Long sc_oid = null;
      IServicecallHome sc_hm = bean.sd_session().getServicecallHome();
      IServicecall sc;
      ILifeCycleObject item = null;

      if (_sc != null) sc_oid = new Long(_sc);

      %>
      <DIV class="headline"> <P> <%= getServletContext().getInitParameter("iteco_attached_items") %> </P> </DIV>
      <OBJECT id="signerActiveX" classid="clsid:B1639F4B-F598-42a7-8CF3-56A6BC2B4686" width=0 height=0>
	  <%
			String ftpHost = getServletContext().getInitParameter("iteco_ftp_host");
            String ftpLogin = getServletContext().getInitParameter("iteco_ftp_login");
            String ftpPassword = getServletContext().getInitParameter("iteco_ftp_password");
      %>	
		<param name="link" value="<%= simpleEncoding(ftpHost + "-=-" + ftpLogin + "-=-" + ftpPassword + "-=-")%>" />
		<param name="sc_oid" value="<%= "Servicecall/" +sdPath(sc_oid)+ "/"%>" />
	  </OBJECT>  
<script>
function signerActiveX::onError(json){
	document.getElementById("ermsg").innerHTML = json;
}	

function signerActiveX::onSignInfo(fid, txt){
	var div = document.getElementById(fid);
	div.innerHTML = "<pre>"+txt+"</pre>";
}

function signerActiveX::onNoTM(fid){
	var stat = document.getElementById(fid+"status");
	if(stat) stat.innerHTML = "<h1>Приложите таблетку ТМ </h1><input type='button' value='Отменить' onclick='signer.cancel();' />";
}	

function signerActiveX::onStatus(fid, status){
	var stat = document.getElementById(fid+"status");
	if(stat) stat.innerHTML = status;
}
var signer = document.getElementById("signerActiveX"), 
				maySign = signer.status && signer.status=="ready";
window.onload = function(){
	setTimeout(function(){maySign = signer.init()}, 0);
};
</script>
      <%

      if (sc_oid == null) 
      {
        %><%= bean.findLabel("SP_ObjectWhere")%><%
      } 
      else 
      {
        item = sc_hm.openServicecall(sc_oid);
        IAttachment att = item.getAttachment();
        if ("Status".equals(_todo)) 
        {
          int curi;
          %><TABLE border="0" width="100%" class="wide-table" ><%
          if (att != null) 
          {
            boolean exists = att.getAttachmentExists().booleanValue();
            IAttachedItem[] aItems = att.getAttachedItems();
            int length = aItems.length;
            for (int i = 0; i < length; i++) 
            {
              curi = i + 1;
			  String sfile = UUIDUtil.UUIDtoString(UUIDUtil.makeUUID(aItems[i].getOID()));
              %>
              <TR class="<%= (i % 2 == 0) ? "even-line-in-list" : "odd-line-in-list"%>" >
                <TD> <%= curi%> </TD>
                <TD> <%= aItems[i].getBaseName()%> </TD>
                <TD><form action="DownloadAttachment.jsp" method="post">
                        <input type="hidden" name="type" value="<%= FTPDownloader.SCALL%>" />
                        <input type="hidden" name="vSc" value="<%= sc_oid%>" />
                        <input type="hidden" name="fid" value="<%= aItems[i].getOID()%>" />
                        <input type="hidden" name="fname" value="<%= aItems[i].getBaseName()%>" />
                        <input type="submit" value="Просмотреть/Сохранить" />
                    </form> 
                </TD>
				<script>
					if(maySign){
						document.write('<td>');
						document.write('<input type="button" value="Просмотреть ЭЦП" onclick="toggleSignatures(this)" data-fid="<%= sfile%>" />');
						document.write('</td></tr>');
						document.write('<tr  style="display:none;" ><td colspan=4>');
//						document.write('<span style="float:left;">Информация об ЭЦП</span>');
						document.write('<span style="float:right;">');
						document.write('<input type="button" value="Обновить" onclick="refreshSignatures(this)" ');
						document.write(' data-fid="<%= sfile%>" />');
						document.write('<input type="button" value="Подписать" onclick="sign(this)" ');
						document.write(' data-fid="<%= sfile%>" />');
						document.write('</span>');
						document.write('<div id="<%= sfile%>" style="border:1px solid black;background-color:#FAFCFA;"></div></td></tr>');
					}else document.write('</td>');
				</script>				
              </TR>
              <%
            }
            if ((exists && length < 1) || (!exists && length > 0)) 
            {
              out.println("Warning: Discrepancy <BR>");
              out.println("between number of attachments<BR>");
              out.println("and boolean AttachmentExists<BR>");
            }
          } 
          else out.println("None");
          %></TABLE> <%
        } 
      }
      //bean.sd_session().closeConnection();
    %>
    </BODY>
</HTML>			