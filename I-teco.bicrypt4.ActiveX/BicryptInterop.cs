﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Iteco.bicrypt4
{
    internal static class BicryptInterop{
        private const string BICRYPT_LIBPATH = @"bicr_adm_exp.dll";

        [DllImport(BICRYPT_LIBPATH)]
        public static extern int cr_init(
                Int32 tm_flag,
                ref Int32 init_mode,
                ref IntPtr init_struct);

        [DllImport(BICRYPT_LIBPATH)]
        public static extern int cr_uninit(IntPtr init_struct);

        [DllImport(BICRYPT_LIBPATH)]
        public static extern int cr_init_prnd(IntPtr init_struct, 
                    string filename, 
                    int flag_init_grn);

        [DllImport(BICRYPT_LIBPATH, CharSet = CharSet.Ansi)]
        public extern static int cr_get_version_info(StringBuilder info_str, ref int str_blen);

        [DllImport(BICRYPT_LIBPATH)]
        public static extern int cr_pkbase_close(IntPtr pkbase_struct);

        [DllImport(BICRYPT_LIBPATH)]
        public static extern int cr_pkbase_open(string pk_file,
                                                int com_blen,
                                                int flag_modify,
                                                ref IntPtr pkbase_struct);

        [DllImport(BICRYPT_LIBPATH)]
        public static extern int cr_elgkey_close(IntPtr init_struct, IntPtr user_struct);

        [DllImport(BICRYPT_LIBPATH)]
        public static extern int cr_load_elgkey(IntPtr init_struct,
                    string password,
                    int passblen,
                    byte[] secretKey,
                    int secr_blen,
                    StringBuilder tm_number,
                    ref int tmn_blen,
                    [Out, MarshalAs(UnmanagedType.LPStr)]StringBuilder userid,
                    ref int userid_blen,
                    ref IntPtr user_struct);

        [DllImport(BICRYPT_LIBPATH)]
        public static extern int cr_sign_file(IntPtr init_struct, IntPtr user_struct, string file_name);

        [DllImport(BICRYPT_LIBPATH)]
        public static extern int cr_check_file(IntPtr init_struct,
                        IntPtr pkbase_struct,
                        string file_name,
                        int N,
                        int flag_del,
                        [Out, MarshalAs(UnmanagedType.LPStr)]StringBuilder userid,
                        ref int userid_blen);

        [DllImport(BICRYPT_LIBPATH)]
        public static extern int cr_pkbase_findfirst(IntPtr pkbase_struct,
                    ref IntPtr pkey_struct,
                    StringBuilder comment,
                    ref int com_blen);

        [DllImport(BICRYPT_LIBPATH)]
        public static extern int cr_read_skey(IntPtr init_handle,
                    string password,
                    int passblen,
                    string ConfidentFilename,
                    [Out, MarshalAs(UnmanagedType.LPStr)]StringBuilder userid,
                    ref int userid_blen,
                    ref IntPtr user_struct);

        [DllImport(BICRYPT_LIBPATH)]
        public static extern int cr_load_bicr_dll(string path);

    }
}
