﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Iteco.bicrypt4
{
    [ComVisible(true)]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    [Guid("BC5F3E90-86A1-4147-BE06-67C57733BD0B")]
    interface ICOMmethods
    {
        string status { get; }

        string getVersion();
        bool init();
        void sign(string fid);
        void cancel();
        void getSignInfo(string fid);
        void dispose();
    }

    [ComVisible(true)]
    [Guid("26724EF6-8228-4e76-BE61-35903B767EC3")]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface IComEvents
    {
        [DispId(1)]
        void onError(string text);
        [DispId(2)]
        void onOK(string fid);
        [DispId(3)]
        void onSignInfo(string fid, string text);
        [DispId(4)]
        void onNoTM(string fid);
        [DispId(5)]
        void onStatus(string fid, string text);


    }
}
