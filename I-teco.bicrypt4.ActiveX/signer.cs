﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Net.Sockets;
using System.Configuration;
using BytesRoad.Net.Ftp;

namespace Iteco.bicrypt4
{
    [ComVisible(true)]
    [ClassInterface(ClassInterfaceType.None)]    
    [Guid("B1639F4B-F598-42a7-8CF3-56A6BC2B4686")]
    [ProgId("Iteco.bicrypt4.signer")]
    [ComDefaultInterface(typeof(ICOMmethods))]
    [ComSourceInterfaces(typeof(IComEvents))]    

    public partial class signer : ICOMmethods
    {
        private string _ftpHost, _ftpUser, _ftpPassword, _link, _scoid;
        
        #region javascript events
        [ComVisible(false)]
        public delegate void onErrorHandler(string text);
        public event onErrorHandler onError = null;

        [ComVisible(false)]
        public delegate void onOKHandler(string fid);
        public event onOKHandler onOK = null;

        [ComVisible(false)]
        public delegate void onSignInfoHandler(string fid, string text);
        public event onSignInfoHandler onSignInfo = null;

        [ComVisible(false)]
        public delegate void onNoTMHandler(string fid);
        public event onNoTMHandler onNoTM = null;

        [ComVisible(false)]
        public delegate void onStatusHandler(string fid, string text);
        public event onStatusHandler onStatus = null;

        #endregion

        private string _status = "init";
        #region bicrypt
        protected IntPtr CrStruct;
        protected IntPtr UserStruct;
        protected IntPtr HashStruct;
        protected IntPtr PKBaseStruct;
        protected IntPtr PKKey;
        private StringBuilder UserID;
        private string prndFile, keyFile, keyPass;
        private bool usePrndFile, useTM;
        protected int UserLength;
        List<IntPtr> pkBase = new List<IntPtr>();
        private string[] errCode = {
            "Правильная подпись",
            "Недостаточно памяти",
            "Подпись неверна",
            "Длина буфера неверна",
            "Номер пользователя неверен",
            "Ошибка зашифрования",
            "ERR7",
            "Ошибка декодирования мастер-ключа",
            "используются функции из КСБ-С, которых нет в 4.0",
            "Ошибка контрольной суммы файла с закрытым ключом",
            "Ошибка многопоточности",
            "Нет подписи",
            "Ошибка открытия файла",
            "Ошибка открытия файла с мастер ключом",
            "Ошибка открытия файла с публичным ключом",
            "Ошибка открытия файла с закрытым ключом",
            "Слишком много USB носителей в USB-слоте",
            "Ошибка чтения файла с закрытым ключом",
            "Ошибка чтения файла с мастер-ключом",
            "Идентификатор подписи не зарегистрирован в БОК",
            "внутренние тесты библиотеки проведены с ошибкой",
            "Ошибка чтения главного ключа",
            "Ошибка чтения узла замены",
            "Ошибка контрольной суммы главного ключа",
            "Главный ключ требует ввода пароля",
            "Не найден датчик ДСЧ",
            "Ошибка контрольной суммы при чтении ТМ",
            "Ошибка загрузки библиотеки grn.dll",
            "Остановлено пользователем",
            "Нет драйвера ТМ",
            "Не приложена ТМ к съемнику",
            "Ошибка чтения TM",
            "Ошибка в параметрах функции",
            "Ошибка хэндла (например, обращение к закрытому хэндлу)",
            "Неправильный тип хэндла (например, вместо H_INIT передается H_PKEY)",
            "Ошибка записи ТМ",
            "ERR36",
            "Ошибка чтения файла сетевых ключей",
            "ERR38",
            "Ошибка инициализации хендла",
            "Ошибка загрузки ключа",
            "ERR41",
            "Ошибка сетевого ключа",
            "Буфер не был зашифрован",
            "Ошибка расшифрования буфера",
            "Ошибка файлового ключа",
            "Ошибка чтения файла",
            "Ошибка записи файла",
            "Ошибка компрессии",
            "Ошибка - длина буфера недостаточна"
        };
        #endregion

        public String status{
            get   {return _status;}
        }

        public void dispose()
        {
            _TMcancelled = true;
            BicryptInterop.cr_elgkey_close(CrStruct, UserStruct);
            for (int i = 0; i < pkBase.Count;i++ )
            {
                BicryptInterop.cr_pkbase_close(pkBase[i]);
            }
            BicryptInterop.cr_uninit(CrStruct);
        }

        List<string> VOK = new List<string>();
        List<string> VOKname = new List<string>();
        public bool init() {
            if (onSignInfo == null) ErrMsg("no onSignInfo callback");
            if (onNoTM == null) ErrMsg("no onNoTM callback");

            string runtimeDir = (new FileInfo(Assembly.GetExecutingAssembly().Location)).DirectoryName + "\\";
            string confDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string confFile = confDir +"\\I-teco.bicrypt4.dll.config";
            if(!File.Exists(confFile)) {ErrMsg("нет файла конфигурации"); return false;}
            ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
            fileMap.ExeConfigFilename = confFile;
            System.Configuration.Configuration config = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);

            usePrndFile = Convert.ToBoolean(config.AppSettings.Settings["USE_PRNDFILE"].Value);
            if (usePrndFile)
            {
                prndFile = confDir + "\\prnd.db3";
                usePrndFile = File.Exists(prndFile);
            }

            keyFile = config.AppSettings.Settings["KEY_FILE"].Value;
            keyPass = config.AppSettings.Settings["KEY_PASS"].Value;
            useTM = String.IsNullOrEmpty(keyFile) || String.IsNullOrEmpty(keyPass);
            if (!useTM)
            {
                useTM = !File.Exists(keyFile);
                if (useTM)
                {
                    keyFile = runtimeDir + keyFile;
                    useTM = !File.Exists(keyFile);
                }
            }
            
            int refInitMode = 0, res;
            res = BicryptInterop.cr_load_bicr_dll(runtimeDir);
            if (res != 0) ErrMsg("загрузка библиотек Бикрипта " + errCode[res]);
            res = BicryptInterop.cr_init(
                (useTM)? 1:0,
                ref refInitMode,
                ref CrStruct);
            if (res != 0) ErrMsg("инициализация Бикрипта "+errCode[res]);

            string[] vokfile = config.AppSettings.Settings["VOK"].Value.Split(';');
            string[] vokname = config.AppSettings.Settings["VOKname"].Value.Split(';');
            for(int i=0;i<vokfile.Length;i++) {
               if (vokfile[i].Trim().Equals("")) continue;
               if (File.Exists(vokfile[i]))
               {
                   VOK.Add(vokfile[i]);
                   VOKname.Add(
                    (vokfile.Length == vokname.Length && !vokname[i].Trim().Equals("")) ?  
                    vokname[i] : vokfile[i]
                   );
               }
               else MessageBox.Show("signerActiveX: " + "недоступен файл базы ключей '" + vokfile[i] + "', игнорируется");               
            }
            if (VOK.Count == 0) ErrMsg("не заданы базы открытых ключей");                
            else for (int i = 0; i < VOK.Count; i++){
                IntPtr pkBasePointer = IntPtr.Zero;
                StringBuilder comment = new StringBuilder(0);
                IntPtr keyPtr = IntPtr.Zero;
                int len = 0;
                res = BicryptInterop.cr_pkbase_open(VOK[i], 0, 0, ref pkBasePointer);
                if(res == 0) res =  BicryptInterop.cr_pkbase_findfirst(pkBasePointer, ref keyPtr, comment, ref len);
                if (res == 0) pkBase.Add(pkBasePointer);
                else ErrMsg(VOK[i] + " - это не файл базы ключей"); 
            }

            return _status.Equals("ready");
        }

        public string getVersion() {
            int capacity = 80;
            var version = new StringBuilder(capacity);
            int err = BicryptInterop.cr_get_version_info(version, ref capacity);
            if (err != 0) return "Error: " + errCode[err];
            else return version.ToString();
        }

        public void getSignInfo(string fid)
        {
            onStatus(fid, "Проверка ЭЦП");
            BackgroundWorker signInfoWorker = new BackgroundWorker();
            signInfoWorker.DoWork += (sender, eventArgs) => {
                FtpClient client = new FtpClient();
                try
                {
                    client.PassiveMode = false;
                    client.Connect(0, _ftpHost, 21);
                    client.Login(0, _ftpUser, _ftpPassword);
                    using (var tmp = new TempFile())
                    {
                        client.GetFile(0, tmp.Path, _scoid + fid);
                        onSignInfo(fid, getSignatures(tmp.Path));  
                    }
                }
                catch (Exception e) { ErrMsg(e.Message+"\ninner\n"+e.InnerException.ToString()+"\ntrace\n"+e.StackTrace); }
                finally
                {
                    if(client.IsConnected) client.Disconnect(0);
                    client.Dispose();
                }

            };
            signInfoWorker.RunWorkerCompleted += (sender, eventArgs) =>
            {
                              
            };

            signInfoWorker.RunWorkerAsync();
        }

        private string getSignatures(string file)
        {
            int len = 1024;
            StringBuilder userId = new StringBuilder(len);
            StringBuilder ret = new StringBuilder();
            for (int N = 1; ; N++)
            {
                int i;
                for (i = 0; i < pkBase.Count; i++)
                {
                    len = 1024;
                    userId = new StringBuilder(len);                    
                    int result = BicryptInterop.cr_check_file(CrStruct,
                        pkBase[i],
                        file,
                        N,
                        0,
                        userId,
                        ref len
                    );
//                    ErrMsg("i=" + i + " N=" + N + " ret=" + ret.ToString());
                    if (result == 11) { string sret = ret.ToString(); return String.IsNullOrEmpty(sret) ? "нет ЭЦП" : sret; } //подписи нет в файле
                    //                if(result == 19) break; идентификатор пользователя владельца подписи незарегистрирован в справочнике публичных ключей
                    if (result == 2)
                    { //подпись неверна
                        ret.Append("ЭЦП '" + Encoding.GetEncoding(866).GetString(Encoding.Default.GetBytes(userId.ToString())) + "'\t неверна\n");
                        break;
                    }
                    if (result == 0)
                    {
                        ret.Append("ЭЦП '" + Encoding.GetEncoding(866).GetString(Encoding.Default.GetBytes(userId.ToString())) + "'\t истинная, БОК '" + VOKname[i] + "'\n");
                        break;
                    }
                }
                if (i == pkBase.Count) ret.Append("ЭЦП '" + Encoding.GetEncoding(866).GetString(Encoding.Default.GetBytes(userId.ToString())) + "' не зарегистрирована в БОК \n");
            }
        }


        public void sign(string fid)
        {
            _TMcancelled = false;
            BackgroundWorker signWorker = new BackgroundWorker();
            signWorker.DoWork += (sender, eventArgs) => {
                while (!_TMcancelled)
                {
                    onStatus(fid, "Инициализация Бикрипта");
                    int result = BicryptInterop.cr_init_prnd(
                        CrStruct,
                        usePrndFile ? prndFile: "",
                        1);
// ERR_NO_TM_ATTACHED	30
// ERR_READ_TM	31
// ERR_WRITE_TM	35
                    if(result == 30 || result == 31 || result == 35){
                        onNoTM(fid);
                        Thread.Sleep(500);
                        signWorker.ReportProgress(0);
                        continue;
                    }
                    if(result != 0){
                        ErrMsg("инициализация ДСЧ: "+errCode[result]); 
                        break;
                    }
           
                    string password = null;
                    int passLength = 0;
                    int tmNumberLength = 128;
                    byte[] secKey = null;
                    var tmNumber = new StringBuilder(tmNumberLength);
                    UserLength = 128;
                    UserID = new StringBuilder(UserLength);
                    if(useTM){
                        result = BicryptInterop.cr_load_elgkey(CrStruct,
                                password,
                                passLength,
                                secKey,
                                0,
                                tmNumber,
                                ref tmNumberLength,
                                UserID,
                                ref UserLength,
                                ref UserStruct);
                    }else{
                        result = BicryptInterop.cr_read_skey(CrStruct,
                            keyPass,
                            keyPass.Length,
                            keyFile,
                            UserID,
                            ref UserLength,
                            ref UserStruct);
                    }
                    if (result != 0) {
                        ErrMsg("загрузка ключа ЭЦП: "+errCode[result]);
                        break;
                    }

                    onStatus(fid, "Загрузка файла");
                    FtpClient client = new FtpClient();
                    try
                    {
                        client.PassiveMode = false;
                        client.Connect(0, _ftpHost, 21);
                        client.Login(0, _ftpUser, _ftpPassword);
                        using (var tmp = new TempFile())
                        {
                            client.GetFile(0, tmp.Path, _scoid + fid);
                            result = BicryptInterop.cr_sign_file(CrStruct,
                                UserStruct,
                                tmp.Path);
                            if (result != 0)
                            {
                                ErrMsg("ошибка подписывания: " + errCode[result]);
                                break;
                            }
                            client.PutFile(0, _scoid + fid, tmp.Path);
                            break;
                        }
                    }
                    catch (Exception e) { ErrMsg(e.Message); }
                    finally {
                        if (client.IsConnected) client.Disconnect(0);
                        client.Dispose();
                    }
                }                
            };
            signWorker.WorkerSupportsCancellation = true;
            signWorker.WorkerReportsProgress = true;
            signWorker.ProgressChanged += (sender, eventArgs) => {
            };
            signWorker.RunWorkerCompleted += (sender, eventArgs) => {
                getSignInfo(fid);
            };
            signWorker.RunWorkerAsync();
        }

        public bool _TMcancelled = false;
        public void cancel(){
            _TMcancelled = true;
        }

    }
}
