﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Configuration;
using System.Windows.Forms;

namespace Iteco.bicrypt4
{
    [RunInstaller(true)]
    public partial class Installer1 : Installer
    {
        public Installer1()
        {
            InitializeComponent();
        }
        public override void Uninstall(IDictionary savedState)
        {
            base.Uninstall(savedState);
        }

        public override void Install(System.Collections.IDictionary stateSaver)
        {
            base.Install(stateSaver);
            string confFile = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\I-teco.bicrypt4.dll.config";
            if (!File.Exists(confFile))  ErrMsg("нет файла конфигурации"); 
            ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
            fileMap.ExeConfigFilename = confFile;
            System.Configuration.Configuration config = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);

            string targetDirectory = Context.Parameters["targetdir"];
            string VOK = Context.Parameters["VOK"];
            string VOKname = Context.Parameters["VOKname"];
            bool USE_PRNDFILE = Context.Parameters["USE_PRNDFILE"].Equals("1");
//            string exePath = string.Format("{0}MyWindowsFormsApplication.exe", targetDirectory);

            config.AppSettings.Settings["VOK"].Value = VOK;
            config.AppSettings.Settings["VOKname"].Value = VOKname;
            config.AppSettings.Settings["USE_PRNDFILE"].Value = USE_PRNDFILE.ToString();
            config.Save();
        }
        private void ErrMsg(string msg)
        {
            MessageBox.Show("signerActiveX installer: " + msg);
        }

    }
}
