﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Iteco.bicrypt4
{
    [ComImport()]
    [Guid("0000010c-0000-0000-C000-000000000046")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IPersist
    {
        void GetClassID(out Guid pClassID);
    };

    [ComImport()]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [Guid("37D84F60-42CB-11CE-8135-00AA004BB851")]
    public interface IPersistPropertyBag : IPersist
    {
        [PreserveSig]
        new void GetClassID(out Guid pClassID);
        [PreserveSig]
        void InitNew();
        [PreserveSig]
        void Load
        (
            [In, MarshalAs(UnmanagedType.IUnknown)] object pPropBag, // IPropertyBag*
            [In, MarshalAs(UnmanagedType.IUnknown)] object pErrorLog // IErrorLog*
        );
        [PreserveSig]
        int Save
        (
            /* [in] */ ref object pPropBag, // IPropertyBag*
            /* [in] */ bool fClearDirty,
            /* [in] */ bool fSaveAllProperties
        );
    };

    [ComImport()]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [Guid("55272A00-42CB-11CE-8135-00AA004BB851")]
    public interface IPropertyBag
    {
        [PreserveSig]
        int Read
        (
            [In, MarshalAs(UnmanagedType.BStr)] String bstrPropName,
            [In, Out] ref object pVar, // VARIANT __RPC_FAR *pVar
            [In] object pErrorLog // IErrorLog* pErrorLog
        );
        [PreserveSig]
        int Write
        (
            /* [in] */ string pszPropName,
            /* [in] */ ref object pVar // VARIANT* pVar
        );
    };

        public partial class signer : IPersistPropertyBag
        {
            // IPersist            
            public void GetClassID(out Guid pClassID)
            {
                pClassID = GetType().GUID;
            }

            // IPersistPropertyBag            
            public void InitNew()
            {
            }


            public void Load
            (
                /* [in] */ object pPropBag, // IPropertyBag*
                /* [in] */ object pErrorLog // IErrorLog*
            )
            {
                IPropertyBag pIPropertyBag = (IPropertyBag)pPropBag;
                object var = null;
//case of unencrypted parameters
                if (pIPropertyBag.Read("sc_oid", ref var, pErrorLog) == 0)
                    _scoid = (string)var;
                else ErrMsg("'sc_oid' init parameters not found");

                if (pIPropertyBag.Read("ftpHost", ref var, pErrorLog) == 0)
                    _ftpHost = (string)var;
                
                if( pIPropertyBag.Read("ftpLogin", ref var, pErrorLog) ==0)
                            _ftpUser = (string)var;
                
                if( pIPropertyBag.Read("ftpPassword", ref var, pErrorLog) == 0)
                            _ftpPassword = (string)var;
                if (String.IsNullOrEmpty(_ftpHost) || String.IsNullOrEmpty(_ftpUser) || String.IsNullOrEmpty(_ftpPassword))
                {
                    //case of encrypted parameters
                    if (pIPropertyBag.Read("link", ref var, pErrorLog) == 0)
                        _link = (string)var;
                    else ErrMsg("'link' init parameters not found");
                    props data = decryptLink(_link);
                    if (data == null) ErrMsg("failed decription of init parameters");
                    else { 
                        _ftpHost = data.host; 
                        _ftpUser = data.user; 
                        _ftpPassword = data.password;
                    }
                }
                if (_status == "init") _status = "ready";
            }
            private void ErrMsg(string msg){
                MessageBox.Show("signerActiveX: " + msg);
                _status = msg;
            }
            private class props{
                public string host;
                public string user;
                public string password;
            }
            private props decryptLink(string link){
                var ret = new props();
             	int[] transposition = {4,7,3,0,6,9,2,15,22,10,1,13,8,14,23,28,16,27,17,25,18,19,5,20,21,24,26,11,29,12 };
			    char[] syma = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM0123456789,.-@#$%^&*()+=".ToCharArray();
			    string subst = "PsR5Zi2LNg0Hl(p&S7oUI$YWv#C-3MmOz%^Jkb1,we*VFEd+@TA)t9r4GnQ.qxh6=8ujcXyDKfBa";
              	char[] inp = link.ToCharArray();			
			    int len = (transposition.Length > inp.Length)?transposition.Length: inp.Length;
			    char[] outp = new char[len];
                for (int i=0; i<outp.Length; i++) { outp[i] = ' '; }
    			for (int i = 0; i < inp.Length; i++) {								
				    int sidx = subst.IndexOf( inp[i]);
				    char cs = (sidx != -1)? syma[sidx]: inp[i];
				    int tidx = Array.IndexOf(transposition, i);			
    				tidx = (tidx == -1)?i:tidx;
	    			outp[tidx] = cs;
			    }			
			    string decr = new String(outp);
                string[] parts = Regex.Split(decr, @"\-\=\-");
                if (parts.Length < 3) return null;
                ret.host = parts[0];
                ret.user = parts[1];
                ret.password = parts[2];
                return ret;
            }

            public int Save
            (
                /* [in] */ ref object pPropBag, // IPropertyBag*
                /* [in] */ bool fClearDirty,
                /* [in] */ bool fSaveAllProperties
            )
            {
                return 0;
            }

        }

}
