Option Explicit
If (Wscript.Arguments.Count < 1) Then
Wscript.Echo "Windows Installer utility to execute SQL queries against an installer database." & vbCRLf & " The 1st argument specifies the path to the MSI database, relative or full path"
Wscript.Quit 1
End If
Dim openMode : openMode = 1 'msiOpenDatabaseModeTransact
On Error Resume Next
Dim installer : Set installer = Wscript.CreateObject("WindowsInstaller.Installer") : CheckError
' Open database
Dim database : Set database = installer.OpenDatabase(Wscript.Arguments(0), openMode) : CheckError
Wscript.Echo "Removing all BannerText..."
Dim query
query = "UPDATE `Control` SET `Control`.`Text`='' WHERE `Control`.`Control`='InstalledBannerText' OR `Control`.`Control`='BannerText' OR `Control`.`Control`='RemoveBannerText'"
Dim view : Set view = database.OpenView(query) : CheckError
view.Execute : CheckError
database.Commit
Wscript.Echo "Done."
'
Dim objShell
Set objShell = WScript.CreateObject( "WScript.Shell" )
objShell.Exec("..\\cabarc\\cabarc.exe n Installer.cab ..\\cabarc\\setup.inf Installer.msi")
Set objShell = Nothing
Wscript.Quit 0
Sub CheckError
Dim message, errRec
If Err = 0 Then Exit Sub
message = Err.Source & " " & Hex(Err) & ": " & Err.Description
If Not installer Is Nothing Then
Set errRec = installer.LastErrorRecord
If Not errRec Is Nothing Then message = message & vbCRLf & errRec.FormatText
End If
Wscript.Echo message
Wscript.Quit 2
End Sub